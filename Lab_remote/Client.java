import java.io.Serializable;
import java.util.concurrent.TimeUnit;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;
import java.util.Collections;
import akka.actor.ActorRef;
import akka.actor.UntypedActor;
import akka.actor.ActorSystem;
import akka.actor.Props;
import akka.actor.Cancellable;
import scala.concurrent.duration.Duration;
import com.typesafe.config.ConfigFactory;
import com.typesafe.config.Config;
/**
 * This class is for Client it will accept the commands from the client and sends back the instruction to the NodeApp Class.
 * first it will accept the arguments from the command line and will send it on Recive method through commmessage class and 
 * it will send to the coordinator class based on there arguments. when it recives a data reply or an error from the nodes this
 * class will tell to the client. 
 * use this command "java -cp $AKKA_CLASSPATH:.:.. Client [remote_ip remote_port 'read' or 'write' or 'leave' key value]"
 */
public class Client{
	static private String remotePath = null; 
	static private int MyID;
	
	public static class ClientNode extends UntypedActor{

		public void onReceive(Object message){
			String[] argum;
			String ip=null;
			String port=null;
			String key_alue=null;
			String value=null;
			String key=null;
			if( message instanceof CommMessage){

				CommMessage recMes = ((CommMessage)message);
				if(recMes.flag_message.equals("arguments")){  
					argum=recMes.getMessage();
					ip =argum[0];
					port =argum[1];
					remotePath = "akka.tcp://mysystem@"+ip+":"+port+"/user/node";
					switch (argum.length){
						case(3):
						//leave
						CommMessage sendMes_1 = new CommMessage(argum[0],argum[1],argum[2]);
						System.out.println("leave command from arguments");
						getContext().actorSelection(remotePath).tell(sendMes_1,getSelf());

						break;
						case(4):
						//read
						CommMessage sendMes_2 = new CommMessage(argum[0],argum[1],argum[2],argum[3]);
						System.out.println("read command from arguments");
						getContext().actorSelection(remotePath).tell(sendMes_2,getSelf());

						break;
						case(5):
						//write
						CommMessage sendMes_3 = new CommMessage(argum[0],argum[1],argum[2],argum[3],argum[4]);
						System.out.println("write command from arguments");
						getContext().actorSelection(remotePath).tell(sendMes_3,getSelf());

						break;
					}
				}
				else if(recMes.flag_message.equals("ReadDone")){
					System.out.println("The Data is : " + recMes.message);
				}
				else if(recMes.flag_message.equals("Timeout")){
					System.out.println("The Time out occured");				
				}
				
			}		
			else 
				unhandled(message);
		}
	}

	/**
	* main method will accept the commands from the argument and will send it to on recive method by using the coommessage class.
	*/
	public static void main(String[] args){
		
		if(args.length == 3 && args[2].equals("leave") || args.length == 4 && args[2].equals("read") ||args.length == 5 && args[2].equals("write")  ){

			   //this is for the reading of the configuration file
			Config config = ConfigFactory.load("application");
				//creating the Actor system
			final ActorSystem system = ActorSystem.create("mysystem",config);
			    //creating the node actor 
			final ActorRef client_node =system.actorOf(Props.create(ClientNode.class),"ClientNode"); 

			client_node.tell( new CommMessage(args),null);
		}
		else 
			System.out.println("Wrong number of arguments: [remote_ip remote_port 'read' or 'write' or 'leave' key value]");

	}
}


