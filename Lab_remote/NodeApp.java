/**
 * Yonas Teodros Tefera and Angesome Atakilty 
 * 16 july 2017
 * Distributed Systems 
 * Distributed Key-Value Store with Data Partitioning and Replication
 */

import java.io.Serializable;
import java.util.concurrent.TimeUnit;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;
import java.util.Collections;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.Map.Entry;
import java.io.*;
import akka.actor.ActorRef;
import akka.actor.UntypedActor;
import akka.actor.ActorSystem;
import akka.actor.Props;
import akka.actor.Cancellable;
import scala.concurrent.duration.Duration;
import com.typesafe.config.ConfigFactory;
import com.typesafe.config.Config;
import java.util.Iterator;

public class NodeApp {
	static private String remotePath = null;/**< Akka path of the bootstrapping peer. */
	static public String clientPath = null;	/**< Akka path of the Client . */
	static private int myId; /**< ID of the local node */
	static public int reply_counter=0;
	static public int counter=0;
	static public boolean write_falg=false;	
	static private ActorRef client=null;	
	static public  String flag=null;
	static public Integer N = 2; /**< replication parameter N */
	static public Integer R = 1; /**< R  specify the Read quorum */
	static public Integer W = 2; /**< W  specify the write quorum. R+W > N  */
	
	public static class Join implements Serializable {
		int id;
		public Join(int id) {
			this.id = id;
		}
	}
	/**
	* This class is for asking the data from the other nodes after joing. 
	*/
	public static class datarequest implements Serializable {
		int id;
		ActorRef clinet_add;
		Map<Integer, ActorRef> new_nodes;
		public datarequest(int id,ActorRef clinet,Map<Integer, ActorRef> new_nod) {
			this.id = id;
			this.clinet_add = clinet;
			this.new_nodes = new_nod;
		}
	}
	/**
	 * write request Serializable class for requesting only the replicated nodes to write the data
	 *
	 */ 
	public static class Write_Request implements Serializable {
		String value;
		int version;
		int key;
		
		public Write_Request() {
			this.version = 0;
			this.value= null;
		}
		public Write_Request(int key,String valu) {
			this.value=valu;
			this.key=key;
		}
		
		public Write_Request(String valu,int vers) {
			this.version = vers;
			this.value=valu;
		}
	}
	/**
	 * a serializable class for sending a conirmation messsage to the coordinator node.
	 */  
	public static class Write_confirm implements Serializable {
		String value;
		int version;
		int key;
		
		public Write_confirm() {
			this.version = 0;
			this.value= null;
		}
		public Write_confirm(int key,String valu) {
			this.value=valu;
			this.key=key;
		}
		public Write_confirm(int key,String valu,int vers) {
			this.value=valu;
			this.key=key;
			this.version = vers;
		}

		public Write_confirm(String valu,int vers) {
			this.version = vers;
			this.value=valu;
		}
	}
	/**
	 * Join write confirm class is a Serializable message class for sending the data to the newely joind class
	 * so that the joined node can save the highst version data.
	 */ 
	public static class Join_Write_confirm implements Serializable {
		String value;
		int version;
		int key;
		
		public Join_Write_confirm() {
			this.version = 0;
			this.value= null;
		}
		public Join_Write_confirm(int key,String valu) {
			this.value=valu;
			this.key=key;
		}
		public Join_Write_confirm(int key,String valu,int vers) {
			this.value=valu;
			this.key=key;
			this.version = vers;
		}

		public Join_Write_confirm(String valu,int vers) {
			this.version = vers;
			this.value=valu;
		}
	}
	/**
	* The coordienator class will ask the replicated nodes through this serializable message class.
	*
	*/
	public static class Read_Request implements Serializable {
		int node;
		int version;
		String ip;
		String port;
		
		public Read_Request(int nod,int vers) {
			this.node = nod;
			this.version=vers;
		}
		public Read_Request(int nod,int vers,String ip, String port){
			this.node = nod;
			this.version=vers;
			this.port=port;
			this.ip=ip;
		}
	}
	public static class Leave_Request implements Serializable {
		int Id;
		public Leave_Request(int id) {
			this.Id = id;
		}
	}
	
	public static class Timeout implements Serializable {}
	/**
	*Recovery Class to ask a recovery to the coordinator.
	*/
	public static class Recovery implements Serializable {}
	/**
	*RequestNodelist to ask the node list
	*/
	public static class RequestNodelist implements Serializable {}
	/**
	*this class for the node list to put every node in the has map list
	*
	*/
	
	public static class Nodelist implements Serializable {
		Map<Integer, ActorRef> nodes;
		public Nodelist(Map<Integer, ActorRef> nodes) {
			this.nodes = Collections.unmodifiableMap(new HashMap<Integer, ActorRef>(nodes)); 
		}
	}
	/**
	*Data class for data representation
	*Every node should store the data that belongs to itself using hashmap 
	*Data class will provide a way of storing the key, version and message together and 
	*in the hash map value we can save it as a data object.
	*/
	public static class Data{
		private int version;
		private int key;
		private String value;

		public Data(){
			this.key=0;
			this.value = null;
			this.version = 0;
		}

		public Data(int key,String value, int version){
			this.key=key;
			this.value = value;
			this.version = version;
		}
		public String getMessage() {
			return value;
		}
		public String toString() {
			return "Value = " + this.value + ", version = " + this.version;
		}
		public Integer getVersion() {
			return this.version;
		}
		public Integer getKey() {
			return this.key;
		}
		public String getvalu() {
			return this.value;
		}

	}
	/**
	*The Node Actore class uses this class to create and in herite the actore properties.
	*/
	public static class Node extends UntypedActor {
		final static int TIME_OUT = 1000;
		static public boolean write_flag=false;
		static public boolean read_flag=false;		
	    static public boolean recover_flag=false;
		// The table of all nodes in the system id->ref
		private Map<Integer, ActorRef> nodes = new HashMap<>();
		private Map<Integer, ActorRef> tempnodes = new HashMap<>();
		private Map<Integer, String>  data_to_send= new HashMap<>();
		// Temporary Hash map for cheking the version
		private Map<Integer, String>  temp_data = new HashMap<>();
		private Map<Integer, Data> store_map =  new HashMap<Integer,Data>();
		private Map<Integer, Boolean> node_check =  new HashMap<Integer,Boolean>();
		/**
		* to schedule a Timeout message in a given time
		*/ 
		void setTimeout(int time) {
			getContext().system().scheduler().scheduleOnce(
				Duration.create(time, TimeUnit.MILLISECONDS),
				getSelf(),
					new Timeout(), // the message to send
					getContext().system().dispatcher(), getSelf()
					);
		}
		/**
		*This method is to sort the nodes based on the value (ActorRef).
		*and return the sorted node back
		*/
		private static Map<Integer,ActorRef> get_sorted_nodes(Map<Integer,ActorRef> unsortMap){

			List<Entry<Integer, ActorRef>> list = new LinkedList<Entry<Integer,ActorRef>>(unsortMap.entrySet());

			Collections.sort(list, new Comparator<Entry<Integer, ActorRef>>()
			{
				public int compare(Entry<Integer,ActorRef> o1,Entry<Integer,ActorRef> o2)
				{

					return o1.getKey().compareTo(o2.getKey());

				}
			});

		    // Maintaining insertion order with the help of LinkedList
			Map<Integer,ActorRef> sortedMap = new LinkedHashMap<Integer,ActorRef>();
			for (Entry<Integer,ActorRef> entry : list)
			{
				sortedMap.put(entry.getKey(), entry.getValue());
			}

			return sortedMap;
		}
		/**
		*The get_next_nodes method is for returning the next N nodes from the reference node.
		*
		*/ 
		public static Map<Integer,ActorRef> get_next_nodes(Map<Integer,ActorRef> map,int key){
			int coun=1;
			int key_search=key;
			int n=NodeApp.N;
			Map<Integer,ActorRef> next_nodes = new LinkedHashMap<Integer,ActorRef>();
				//System.out.println("Total number of nodes are  : " + map.size());
			for (Entry<Integer,ActorRef> entry : map.entrySet())
			{	
				if( key_search<entry.getKey() && coun <= n){
					next_nodes.put(entry.getKey(), entry.getValue());
					coun=coun+1;				
				}					
			}
			if(coun!=0 && coun <= n){
				for (Entry<Integer,ActorRef> entry : map.entrySet())
				{	
					if(coun <= n){
						next_nodes.put(entry.getKey(), entry.getValue());
						coun=coun+1;				
					}						
				}
			}
			return next_nodes;
		}
		/**
		* This function will return the replicated nodes by accepting the key.
		*/ 
		public static Map<Integer,ActorRef> get_replication_nodes(Map<Integer,ActorRef> map,int key){
			int coun=1;
			int key_search=key;
			int n=NodeApp.N;
			Map<Integer,ActorRef> replica_nodes = new LinkedHashMap<Integer,ActorRef>();
				//System.out.println("Total number of nodes are  : " + map.size());
			for (Entry<Integer,ActorRef> entry : map.entrySet())
			{	
				if( key_search<=entry.getKey() && coun <= n){
					replica_nodes.put(entry.getKey(), entry.getValue());
					coun=coun+1;				
				}					
			}
			if(coun!=0 && coun <= n){
				for (Entry<Integer,ActorRef> entry : map.entrySet())
				{	
					if(coun <= n){
						replica_nodes.put(entry.getKey(), entry.getValue());
						coun=coun+1;				
					}						
				}
			}
			return replica_nodes;
		}
		/**
		*this functions is to print out the sort nodes 
		*/ 
		public static void printsortedNodes(Map<Integer,ActorRef> map){

			for (Entry<Integer,ActorRef> entry : map.entrySet())
			{
				System.out.println("Key : " + entry.getKey() + " Value : "+ entry.getValue());
			}
		}
		
		/**
		* To print the storedData from the hashmap use this funtion.  
		*/
		public void printData(){
			String val=null;
			System.out.println("reading the stored data");
				//retrieving values from map
			for (Map.Entry<Integer, Data> entry : store_map.entrySet()) {
				System.out.println("key : "+entry.getKey() + ":" + entry.getValue().toString());

			}

		}
		/**
		*This is a file reader function for recovery purpose it will read the data from the file and 
		*save it back to the hashmap.
		*/
		public void Recover_File(){
			Data da= new Data();				

			try{
				FileInputStream filestream = new FileInputStream("./File.txt");
				BufferedReader br = new BufferedReader(new InputStreamReader(filestream));
				StringBuilder sb = new StringBuilder();
				String line;
				while ((line = br.readLine()) != null) {
					String[] text = line.split(" ");
					da= new Data(Integer.valueOf(text[0]),text[1],Integer.valueOf(text[2]));
					store_map.put(Integer.valueOf(text[0]),da);
				}
				br.close();
			}catch(Exception e){
				System.out.println("Error  ");
			}
			System.out.println("Recovered files");
			recover_flag=false;	
		}
		public void preStart() {
			if (remotePath != null && flag==null) {
				getContext().actorSelection(remotePath).tell(new RequestNodelist(), getSelf());
			}
			else if(remotePath != null && flag.equals("join"))
			{
				getContext().actorSelection(remotePath).tell(new RequestNodelist(), getSelf());
				System.out.println("Node " + nodes.size() + ":");
				
			}
			else if(remotePath != null && flag.equals("recover"))
			{
				recover_flag=true;
				getContext().actorSelection(remotePath).tell(new RequestNodelist(), getSelf());
				Recover_File();

			}
			nodes.put(myId, getSelf());
		}

		public void onReceive(Object message) {
			
			if (message instanceof RequestNodelist) {
				getSender().tell(new Nodelist(nodes), getSelf());
			}
			else if (message instanceof Nodelist) {
				nodes.putAll(((Nodelist)message).nodes);
				for (ActorRef n: nodes.values()) {
					n.tell(new Join(myId), getSelf());
				}
			}
			else if (message instanceof Join) {
				int id = ((Join)message).id;
				System.out.println("Node " + id + " joined");
				nodes.put(id, getSender());
				if(write_falg==true && recover_flag==false){
					Map<Integer,ActorRef> sortednodes = get_sorted_nodes(nodes);
					Map<Integer,ActorRef> next_nodes = get_next_nodes(sortednodes,id);
					for (ActorRef n: next_nodes.values()) {
						n.tell(new datarequest(id,getSender(),nodes), getSelf());
					}
				}
			}
			else if (message instanceof CommMessage)
			{
				CommMessage client_Message= (CommMessage)message;
				int key = ((CommMessage)message).key;
				Map<Integer,ActorRef> sortednodes = get_sorted_nodes(nodes);
				Map<Integer,ActorRef> replicated_Nodes = get_replication_nodes(sortednodes,key);
				/**
				*if the message is for writing instruction the coordinator will accept the message and ask the replicated nodes 
				*in addition to this it set a time to check if they replyed within a specified time.
				*/
				if((client_Message.message).equals("read")){ 
					System.out.println("Read Message Recived from client with the key " + client_Message.key + ".");
					Read_Request readrequest = new Read_Request(client_Message.key,1,client_Message.ip,client_Message.port);
					client=getSender();
					for(ActorRef n: replicated_Nodes.values())
					{
						n.tell(readrequest, getSelf());
					}
					setTimeout(TIME_OUT);
					reply_counter=0;
					read_flag=true;
					write_flag=false;
				}
				else if((client_Message.message).equals("leave")){
					System.out.println("Preparing to leave...");
					Map<Integer,ActorRef> tempnodes = get_sorted_nodes(nodes);
					tempnodes.remove(myId);
					tempnodes = get_sorted_nodes(tempnodes);
					Map<Integer,ActorRef> new_replicated_Nodes = get_replication_nodes(tempnodes,key);
					Data d1= new Data();
					Leave_Request leavrequest = new Leave_Request(myId);/**<send leave request for each node from there list */
					for (ActorRef n: nodes.values())
					{
						n.tell(leavrequest, getSelf());
					}					
					for (Map.Entry<Integer, Data> entry : store_map.entrySet()) { /**<send write request for replicated nodes */
						 Write_Request writrequest = new Write_Request(entry.getKey(),entry.getValue().getvalu());
						for (ActorRef n: new_replicated_Nodes.values())
						{
							n.tell(writrequest, getSelf());
						}					
					}
					nodes.remove(myId);
					reply_counter=0;
				}
				else if((client_Message.message).equals("write")){
					System.out.println("write Request recived from client ");
					client = getSender();
					write_falg=true;
					Write_Request writrequest = new Write_Request(client_Message.key,client_Message.value);

					for (ActorRef n: replicated_Nodes.values())
					{
						n.tell(writrequest, getSelf());
					}
					setTimeout(TIME_OUT);
					//timer start
					reply_counter=0;
					read_flag=false;
					write_flag=true;
				}
				else if((client_Message.message).equals("DatajoinReply")){
					System.out.println("Data join Reply ReadReply recived from client ");
					Write_Request writrequest = new Write_Request(client_Message.key,client_Message.value);
					for (ActorRef n: replicated_Nodes.values())
					{
						n.tell(writrequest, getSelf());
					}
				}			
				else if((client_Message.message).equals("WriteReply"))
				{
					reply_counter=reply_counter+1;
					if (reply_counter==W){
						Write_confirm confirmed = new Write_confirm(client_Message.key,client_Message.value);
						// write reply to the client success message 
						for (ActorRef n: replicated_Nodes.values())
						{
							n.tell(confirmed, getSelf());
						}
					}
				}
				else if((client_Message.message).equals("ReadReply"))
				{
					reply_counter=reply_counter+1;
					data_to_send.put(client_Message.version,client_Message.value);
					if (reply_counter==R){
						int maxKey = Collections.max(data_to_send.keySet());			
						CommMessage reply_mess = new CommMessage(maxKey,data_to_send.get(maxKey),"ReadDone");
						client.tell(reply_mess,getSelf());
						data_to_send.clear();
					}
				}
				else if((client_Message.message).equals("Timeout"))
				{
					System.out.println("Timeout happend sending to "+ client);
					CommMessage reply = new CommMessage("Time","Timeout");
					//getContext().actorSelection(clientPath).tell(reply, getSelf());					
					client.tell(reply,getSelf());
				}					
			}
			else if (message instanceof Timeout) {/* Timeout */
				if (write_flag==true && reply_counter<W) {

					CommMessage reply = new CommMessage("Timeout","Time");
					getSender().tell(reply, getSelf());

				}
				else if(read_flag==true && reply_counter<R){

					CommMessage reply = new CommMessage("Timeout","Time");
					getSender().tell(reply, getSelf());
				}	
			}					
			else if(message instanceof Write_Request){
				CommMessage reply_message = new CommMessage(myId,((Write_Request)message).key,((Write_Request)message).value,"WriteReply");					
				getSender().tell(reply_message, getSelf());

			}
			else if(message instanceof Write_confirm){
				System.out.println("Data writing in progress... !");
				Data d3= new Data();
				Data d1= new Data();
				d3 = store_map.get(((Write_confirm)message).key);
				if (d3 == null)
				{						
					d1= new Data(((Write_confirm)message).key,((Write_confirm)message).value,1);
				}
				else
				{
					d1= new Data(((Write_confirm)message).key,((Write_confirm)message).value,d3.getVersion()+1);
				}
				store_map.put(((Write_confirm)message).key,d1);
				//store to file
				printData();
				try {
					File file = new File("./File.txt");
					if (!file.exists()) {
						file.createNewFile();
					}
					FileWriter fW= new FileWriter(file);
					for (Map.Entry<Integer, Data> entry : store_map.entrySet()) {
								//fW.write("");
						fW.write(entry.getValue().getKey() + " " + entry.getValue().getvalu() + " " + entry.getValue().getVersion() + "\r\n");
					}	
					fW.flush();
					fW.close();
					System.out.println("Data writing has been performed successfully to file");
				}
				catch (IOException e) {
					e.printStackTrace();
					System.out.println("Data writing has got Error !");
				}

			}
			else if(message instanceof Join_Write_confirm){
				counter=counter+1;
				temp_data.put(((Join_Write_confirm)message).version,((Join_Write_confirm)message).value);
				if ((counter+1) ==N){
					int maxKey = Collections.max(temp_data.keySet());			

					System.out.println("Data writing in progress... !");
					Data d1= new Data();
					d1= new Data(((Join_Write_confirm)message).key,temp_data.get(maxKey),maxKey);
					store_map.put(((Join_Write_confirm)message).key,d1);
					//store to file
					printData();
					try {
						File file = new File("./File.txt");
						if (!file.exists()) {
							file.createNewFile();
						}
						FileWriter fW= new FileWriter(file);
						for (Map.Entry<Integer, Data> entry : store_map.entrySet()) {
							    fW.write(entry.getValue().getKey() + " " + entry.getValue().getvalu() + " " + entry.getValue().getVersion() + "\r\n");
						}	
						fW.flush();
						fW.close();
						System.out.println("Data writing has been performed successfully to file");
					}
					catch (IOException e) {
						e.printStackTrace();
						System.out.println("Data writing has got Error !");
					}
					counter=0;
					temp_data.clear();
				}

			}
			else if(message instanceof Read_Request){
				for (Map.Entry<Integer, Data> entry : store_map.entrySet()) {
					if (entry.getKey()==((Read_Request)message).node)
					{	
						System.out.println("key : "+entry.getKey() + ":"  + entry.getValue().getKey() + " : " +entry.getValue().getvalu() + " :" + entry.getValue().getVersion());
						CommMessage reply_message = new CommMessage(myId,entry.getValue().getKey(),entry.getValue().getvalu(),entry.getValue().getVersion(),"ReadReply");					
						getSender().tell(reply_message, getSelf());
					}
				}	
			}
			else if(message instanceof Leave_Request){
				System.out.println("Removing from list...");
				nodes.remove(((Leave_Request)message).Id);
				System.out.println("The new node list ...");
				printsortedNodes(nodes);	

			}
			else if(message instanceof datarequest){
				System.out.println("datarequest from newely joining node");
				Iterator<Map.Entry<Integer, Data>> itr = store_map.entrySet().iterator();
				while(itr.hasNext())
				{   
					Map.Entry<Integer, Data> entry = itr.next();
					Map<Integer,ActorRef> sort = get_sorted_nodes(((datarequest)message).new_nodes);
					Map<Integer,ActorRef> replicated_Nodes = get_replication_nodes(sort,entry.getValue().getKey());
					System.out.println("The newely replicated nodes for this data are  " + replicated_Nodes);

					if (replicated_Nodes.get(((datarequest)message).id) != null) {
						Join_Write_confirm data_confirmed = new Join_Write_confirm(entry.getValue().getKey(),entry.getValue().getvalu(),entry.getValue().getVersion());
						((datarequest)message).clinet_add.tell(data_confirmed, getSelf());
					}

					if (replicated_Nodes.get(myId)== null){	
						System.out.println("The data " +  entry.getValue().getKey() + "  is no more supported here ");
						itr.remove();  // Call Iterator's remove method.
						printData();
					}				     
				}
			}
			else
            	unhandled(message);	// this actor does not handle any incoming messages
        }
    }

    public static void main(String[] args) {

    	if (args.length >3) {
    		System.out.println("Wrong number of arguments: [remote_ip remote_port]");
    		return;
    	}

		// Load the "application.conf"
    	Config config = ConfigFactory.load("application");
    	myId = config.getInt("nodeapp.id");
    	if (args.length == 2) {
			// Starting with a bootstrapping node
    		String ip = args[0];
    		String port = args[1];
    		// The Akka path to the bootstrapping peer
    		remotePath = "akka.tcp://mysystem@"+ip+":"+port+"/user/node";
    		System.out.println("Starting node " + myId + "; bootstrapping node: " + ip + ":"+ port);
    	}
    	else if(args.length == 3){
    		if(args[0].equals("join")){
    			flag="join";
    			String ip = args[1];
    			String port = args[2];
				// The Akka path to the bootstrapping peer
    			remotePath = "akka.tcp://mysystem@"+ip+":"+port+"/user/node";
    			System.out.println("Joining.... " + ip + ":"+ port);
    		}
    		else if(args[0].equals("recover"))
    		{
    			flag="recover";
    			String ip = args[1];
    			String port = args[2];
				// The Akka path to the bootstrapping peer
    			remotePath = "akka.tcp://mysystem@"+ip+":"+port+"/user/node";
    			System.out.println("Recovery... " + ip + ":"+ port);	
    		}
    		else{
    			System.out.println("Wrong number of arguments please use : [join or recover remote_ip remote_port]");
    		}
    	}
    	else 
    		System.out.println("Starting disconnected node " + myId);
		// Create the actor system
    	final ActorSystem system = ActorSystem.create("mysystem", config);

		// Create a single node actor
    	final ActorRef receiver = system.actorOf(
				Props.create(Node.class),	// actor class 
				"node"						// actor name
				);
    }
}
