import java.io.Serializable;
/**
* CommMessage is a Serializable class this coneverts an object message to a series of bytes,
* so that the object can be easily saved to persistent storage or streamed across a communication link.
* it is acommunication bridge class that facilitate the message sending events between the nodes and client.
*/
public class CommMessage implements Serializable{
	public String[] message_args;
	public String message;
	public String reply_message;
	public String flag_message;
	public String value;
	public String ip;
	public String port;
	public int version;
	public int key;
	public int ID;

	public CommMessage (){
		this.message = null;
		this.value = null;
		this.key = 0;
		this.port = null;
		this.ip = null;	
		this.message= null;
		this.reply_message=null;
		this.ID=0;
	}
	
	public CommMessage (String args[]){
		this.message_args=args;
		this.flag_message  = "arguments";
		
	}
	/**
	* This constactor is for read reply from the coordinator node
	*/
	public CommMessage(String Mess){
		this.flag_message  = Mess;
	}
	public CommMessage(String Mess,String val){
		this.message =Mess;
		this.flag_message  = val;
	}

	public CommMessage(int id,int k,String val,String Mess){
		this.ID= id;
		this.key = k;
		this.message  = Mess;
		this.value = val;
		this.flag_message  = null;
	}
	public CommMessage(int id,String mainmessage,String Mess){
		this.ID= id;
		this.message  = mainmessage;
		this.flag_message  = Mess;
	}
    /**
    *constructor for retrived data to the coordinator
    */
	public CommMessage(int id,int ke,String valu,int vers,String Mess){
		this.ID= id;			
		this.key=ke;
		this.value=valu;
		this.version=vers;
		this.message  = Mess;
	}
	
    /**
    * constractor for leave
    */
	public CommMessage(String ip_Add, String port_number,String Mess){
		this.ip= ip_Add;
		this.port=port_number;
		this.message=Mess;
		this.flag_message  = null;
	}
	/**
	*constactor for read
	*/
	public CommMessage(String ip_Add, String port_number,String Mess,String key_val){
		this.ip= ip_Add;
		this.port=port_number;
		this.message=Mess;
		this.key = Integer.valueOf(key_val);
		this.flag_message  = null;

	}
	/**
	*constractor for write
	*/
	public CommMessage (String ip_Add, String port_number,String Mess,String key_val,String valu){
		this.message = Mess;
		this.ip      = ip_Add;
		this.port    = port_number;
		this.key     = Integer.valueOf(key_val);
		this.value   = valu;
		this.flag_message  = null;	
	}
	/**
	*a method for returning back the argument message to the client on recive message.
	*/
	public String[] getMessage(){
		return message_args;	
	}
	/**
	*a method to set the message variable with a specified message.
	*/
	public void setMessage(String set_message){
		this.message =set_message;	
	}	
	
}

